{-# LANGUAGE TemplateHaskell #-}

-- |
--   Module    : Media.VF.Optics
--   License   : MIT
--   Stability : experimental
--
-- Optics for `VF`.
module Media.VF.Optics where

import Media.VF
import Optics.TH

makePrisms ''VF

makePrisms ''VFC
